import 'package:blacknet/models/index.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/rendering.dart';

/// 创建EventBus
EventBus eventBus = EventBus();

/// 钱包列表
class TxListRefreshEvent {
  BlnTxns tx;
  TxListRefreshEvent(this.tx);
}
// 通用
class RefreshEvent {
  RefreshEvent();
}
