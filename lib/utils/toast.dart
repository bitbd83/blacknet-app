
import 'package:oktoast/oktoast.dart';
import 'package:flutter/material.dart';

/// Toast工具类
class Toast {
  static show(String msg, {duration = 2000}) {
    if (msg == null){
      return;
    }
    showToast(
        msg,
        duration: Duration(milliseconds: duration),
        dismissOtherToast: true
    );
  }

  static show2(String msg, {duration = 6000}) {
    var w = Center(
      child: Container(
        padding: const EdgeInsets.all(5),
        color: Colors.black.withOpacity(0.7),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.add,
              color: Colors.white,
            ),
            Text(
              '添加成功',
              style: TextStyle(color: Colors.white),
            ),
          ],
          mainAxisSize: MainAxisSize.min,
        ),
      ),
    );
    showToastWidget(Stack(
      alignment: AlignmentDirectional.topStart,
      children: [
        Opacity(
          opacity: 0.6,
          child: new ModalBarrier(dismissible: false),
        ),
        w
      ],
    ), position: ToastPosition.center, duration: Duration(milliseconds: duration));
  }

  static cancelToast() {
    dismissAllToast();
  }
}