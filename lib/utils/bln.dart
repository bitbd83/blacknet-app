import 'package:intl/intl.dart';
import 'dart:math';
// import 'package:fixnum/fixnum.dart';

String fomartBalance(double balance, [int len]){
  if(len==null){
    len = 4;
  }
  return (balance / pow(10, 8)).toDouble().toStringAsFixed(len).toString();
  // return num.parse((balance ~/ Int64(pow(10, 8))).toDouble().toStringAsFixed(8)).toString();
}

double realBalance(double balance){
  return (balance / pow(10, 8)).toDouble();
}

String formatDate(DateTime now){
  return new DateFormat('MM-dd hh:mm:ss').format(now);
}

String shortAddress(String address, [int len]){
  // print(address.substring(address.length - 16, address.length));
  if(len==null){
    len = 16;
  }
  return address.substring(0,len) +"..."+address.substring(address.length - len, address.length);
}