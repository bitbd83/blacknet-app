import 'package:blacknet/constants/index.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:blacknet/models/bln.dart';
import 'package:blacknet/db/txns.dart';
import 'package:blacknet/utils/sp.dart';

class TxProvider with ChangeNotifier {

  TxDbHelper _txDb;

  TxProvider(){
      _txDb = TxDbHelper();
  }
  // Txns
  Future<void>setTxns(List<BlnTxns> list) async{
    _txDb.insertUpdates(list);
    // notifyListeners();
  }

  Future<List<BlnTxns>> getTxns(String address, [int page, int type]) async {
    if(type == -1 || type == null){
      return await _txDb.query(address, page);
    }else{
      return await _txDb.query(address, page, type);
    }
  }

  // cancellease
  void syncCancelLease(){
    var txStrs = SpUtil.getStringList("transfer_cancellease");
    if(txStrs != null && txStrs.isEmpty == false){
      notifyListeners();
    }
  }

  void setCancelLease(List<BlnLease> list) {
    SpUtil.putStringList("transfer_cancellease", list.map((i) => jsonEncode(i)).toList());
    notifyListeners();
  }

  List<BlnLease> getCancelLease() {
    var txStrs = SpUtil.getStringList("transfer_cancellease");
    return txStrs.map((i)=> BlnLease.fromJson(jsonDecode(i))).toList();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }
}