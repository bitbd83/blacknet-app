export 'package:blacknet/routers/arguments.dart';

import 'package:fluro/fluro.dart';
import 'package:blacknet/pages/index.dart';
// import 'package:intl/intl.dart';
// theme
import 'package:blacknet/pages/theme/index.dart';
class Routes {
  static Router router;

  static String index = "/";
  static String theme = "/theme";
  static String language = "/language";

  static String accountCreate = "/account/create";
  static String accountCreateStep2 = "/account/create/step2";
  static String accountBackup = "/account/backup";

  static String accountImport1 = "/account/import/1";
  static String accountImport2 = "/account/import/2";
  static String accountImport3 = "/account/import/3";

  static String txDetail = "/transaction/detail";

  static void generate(Router router) {
    Routes.router = router;
    // transaction
    router.define(txDetail, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return TxDetailPage();
    }));
    // account
    router.define(accountImport1, handler: Handler(handlerFunc: (context, params) {
      return AccountRecoverPage();
    }));
    router.define(accountImport2, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return AccountRecoverPage2();
    }));
    router.define(accountImport3, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return AccountRecoverPage3();
    }));
    router.define(accountBackup, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return AccountBackupPage();
    }));
    router.define(accountCreate, handler: Handler(handlerFunc: (context, params) {
      return AccountCreatePage();
    }));
    router.define(accountCreateStep2, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return AccountCreatePage2();
    }));
    router.define(theme, handler: Handler(handlerFunc: (context, params) {
      return ThemePage();
    }));
    router.define(language, handler: Handler(handlerFunc: (context, params) {
      return LanguagePage();
    }));
    // 定义路由
    router.define(Routes.index, transitionType: TransitionType.inFromRight, handler: Handler(handlerFunc: (context, params) {
      return IndexPage();
    }));
  }
} 

abstract class IRouterProvider{
  
  void initRouter(Router router);
}