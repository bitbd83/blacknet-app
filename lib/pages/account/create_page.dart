import 'package:flutter/material.dart';
// routers
import 'package:blacknet/routers/routers.dart';
// i8n
import 'package:blacknet/i18n/i18n.dart';

import 'package:blacknet/pages/account/widgets/password.dart';

class AccountCreatePage extends StatefulWidget {
  @override
  _AccountCreatePage createState() => _AccountCreatePage();
}

class _AccountCreatePage extends State<AccountCreatePage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(S.of(context).craeteWallet),
            Text(S.of(context).createStep1, style: TextStyle(fontSize: 14))
          ],
        )
      ),
      // backgroundColor: Theme.of(context).accentColor,
      body: PasswordSettingWidget(
        text: <Widget>[
          Text(S.of(context).settingPassowrd, style: TextStyle(fontSize: 20))
        ],
        buttonText: S.of(context).nextStep,
        onPressed: (List<String> codes){
          Navigator.pushNamed(context, Routes.accountCreateStep2,
            arguments: AccountPageArguments(
              password: codes.join('')
            ));
        },
      )
    );
  }
}