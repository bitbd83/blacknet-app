import 'package:flutter/material.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:blacknet/pages/home/provider/main.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/global.dart';
import 'package:blacknet/provider/tx.dart';
// models
import 'package:blacknet/models/index.dart';
// index page
import 'package:blacknet/pages/wallet/index.dart';
import 'package:blacknet/pages/me/index.dart';
import 'package:blacknet/pages/transfer/index.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// passcode
import 'package:blacknet/components/password/index.dart';
// api
import 'package:blacknet/components/api/api.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage> with WidgetsBindingObserver {

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();
  // provider
  MainProvider provider = MainProvider(); 

  final _pageController = PageController();
  var _pageList;

  @override
  void initState() {
    super.initState();
    // 添加切换监听
    WidgetsBinding.instance.addObserver(this);
    _pageList = [
      WalletPage(),
      TransferIndexPage(),
      MePage()
    ];
  }

  void _onPageChanged(int index) {
    provider.value = index;
  }
  
  @override
  Widget build(BuildContext context) {
    // 显示密码
    if (Provider.of<GlobalProvider>(context).getPassCodeAuth() == false) {
      // 预加载
      _preload();
      return showPassCode(context);
    }

    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: provider)
      ],
      child: Scaffold(
        bottomNavigationBar: Consumer<MainProvider>(
          builder: (_, provider, __){
            return BottomNavigationBar(
              currentIndex: provider.value,
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.touch_app),
                  title: Text(S.of(context).wallet)
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.all_inclusive),
                  title: Text(S.of(context).transfer)
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline),
                  title: Text(S.of(context).me)
                ),
              ],
              backgroundColor: Theme.of(context).bottomAppBarColor,
              onTap: (int index) {
                _pageController.jumpToPage(index);
              },
            );
          },
        ),
        // 使用PageView的原因参看 https://zhuanlan.zhihu.com/p/58582876
        body: PageView(
          controller: _pageController,
          onPageChanged: _onPageChanged,
          children: _pageList,
          physics: NeverScrollableScrollPhysics(), // 禁止滑动
        )
      )
    );
  }

  void _preload(){
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    // cancel lease
    Api.outLeases(bln.address).then((ls){
      if(mounted){
        Provider.of<AccountProvider>(context).setCancelLeaseAsync(ls);
      }
    });
    // balnace
    Api.getBlance(bln.address).then((balnace){
      if(mounted){
        Provider.of<AccountProvider>(context).setBalanceAsync(balnace);
      }
    });
    // txns
    Api.getTxns(bln.address, 1).then((ls){
      if(mounted){
        Provider.of<AccountProvider>(context).setTxnsAsync(ls);
      }
    });
  }

  Widget showPassCode(BuildContext context) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return PasscodeScreen(
      title: S.of(context).unlockPass,
      passwordEnteredCallback: (String enteredPasscode)async{
        bool isValid = bln.password == enteredPasscode;
        _verificationNotifier.add(isValid);
        if (isValid) {
          await Future.delayed(Duration(milliseconds: 50));
          Provider.of<GlobalProvider>(context).setPassCodeAuth(isValid);
        }
      },
      backgroundColor: Theme.of(context).primaryColor,
      shouldTriggerVerification: _verificationNotifier.stream,  
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("--" + state.toString());
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        Provider.of<GlobalProvider>(context).setInActive(DateTime.now().millisecondsSinceEpoch);
        break;
      case AppLifecycleState.resumed:// 应用程序可见，前台
        int lastTime = Provider.of<GlobalProvider>(context).getInActive();
        if((DateTime.now().millisecondsSinceEpoch - lastTime) > 10 * 1000){
          Provider.of<GlobalProvider>(context).setPassCodeAuth(false);
        }
        break;
      case AppLifecycleState.paused: // 应用程序不可见，后台
        Provider.of<GlobalProvider>(context).setInActive(DateTime.now().millisecondsSinceEpoch);
        break;
      case AppLifecycleState.suspending: // 申请将暂时暂停
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _verificationNotifier.close();
    super.dispose();
  }
}