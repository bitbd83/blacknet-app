import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/global.dart';
// theme
import 'package:blacknet/theme/index.dart';
// models
import 'package:blacknet/models/index.dart';
// roters
import 'package:blacknet/routers/routers.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// utils
import 'package:blacknet/utils/index.dart';

class MePage extends StatefulWidget {
  @override
  _MePage createState() => _MePage();
}

class _MePage extends State<MePage> with AutomaticKeepAliveClientMixin<MePage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  bool _check = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    Locale l = Provider.of<GlobalProvider>(context).getLanguage();
    LanguageModel languageModel = LanguageModel(S.delegate.title(l), l.languageCode, l.countryCode);
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: ListTile(
              contentPadding: ThemeLayout.padding(0, 0, 15, 0),
              title: Text(shortAddress(bln.address), style: TextStyle(color: Theme.of(context).appBarTheme.textTheme.title.color)),
              trailing: new Icon(Icons.content_copy, size: 16),
              onTap: () {
                Clipboard.setData(new ClipboardData(text: bln.address)).then((v){
                  Toast.show(S.of(context).copySuccess);
                });
              },
            ),
            currentAccountPicture: new Image(
              image: new AssetImage('assets/images/logo.png')
            )
          ),
          // ListTile(
          //   title: Text(S.of(context).theme),
          //   leading: new CircleAvatar(child: new Icon(Icons.view_module)),
          //   onTap: () {
          //     Routes.router.navigateTo(context, Routes.theme);
          //   },
          // ),
          new ListTile(
            title: Text(S.of(context).language),
            leading: new CircleAvatar(child: new Icon(Icons.language), foregroundColor: Theme.of(context).accentColor, backgroundColor: Theme.of(context).canvasColor),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(S.of(context).lang(languageModel.titleId)),
                Icon(Icons.keyboard_arrow_right)
              ],
            ),
            onTap: () {
              Routes.router.navigateTo(context, Routes.language);
            },
          ),
          ListTile(
            title: Text(S.of(context).logout),
            leading: new CircleAvatar(
              child: new Icon(Icons.exit_to_app),
              foregroundColor: Theme.of(context).accentColor, 
              backgroundColor: Theme.of(context).canvasColor
            ),
            onTap: () {
              Provider.of<AccountProvider>(context).setCancelLease([]);
              Provider.of<AccountProvider>(context).setBalance(BlnBalance.empty());
              Provider.of<AccountProvider>(context).setCurrentBln(Bln.empty());
            },
            enabled: true,
          ),
          // ListTile(
          //   title: Text("show"),
          //   leading: new CircleAvatar(
          //     child: new Icon(Icons.exit_to_app),
          //   ),
          //   onTap: () {
          //     OkProgressDialog.show();
          //   }
          // ),
          // ListTile(
          //   title: Text("hide"),
          //   leading: new CircleAvatar(
          //     child: new Icon(Icons.exit_to_app),
          //   ),
          //   onTap: () {
          //     OkProgressDialog.hide();
          //   }
          // ),
          // ListTile(
          //   title: Text("test"),
          //   leading: new CircleAvatar(
          //     child: new Icon(Icons.exit_to_app),
          //   ),
          //   onTap: () {
          //     // Navigator.pushNamed(context, Routes.txDetail,
          //     // arguments: TxDetailPageArguments(
          //     //   bln: BlnTxns(
          //     //     hash: 'be02fc434bc4eb73449fa655b84db36187047c8cac62a149c2988c0ae3dede88',
          //     //     txid: 'be02fc434bc4eb73449fa655b84db36187047c8cac62a149c2988c0ae3dede88',
          //     //     from: 'blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj',
          //     //     data: BlnTxnsData(
          //     //       amount: 700000000,
          //     //       to: "blacknet1y36epd3xh7vt2yv2yu7pnk06amfpyqlcc2jlkf4caznmc4xlgqgqc6deuj"
          //     //     ),
          //     //     fee: 100000
          //     //   )
          //     // ));
          //   },
          //   enabled: true,
          // ),
          // new AboutListTile(
          //   icon: new CircleAvatar(child: new Text("Ab")),
          //   child: new Text("About"),
          //   applicationName: "Blacknet Main",
          //   applicationVersion: "1.0",
          //   applicationIcon: new Image.asset(
          //     'assets/images/logo.png',
          //     width: 64.0,
          //     height: 64.0,
          //   ),
          //   applicationLegalese: "applicationLegalese",
          //   aboutBoxChildren: <Widget>[
          //     new Text("BoxChildren"),
          //     new Text("box child 2")
          //   ],
          // ),
        ],
      )
    );
  }
}