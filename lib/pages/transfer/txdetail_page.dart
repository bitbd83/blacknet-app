import 'dart:async';
import 'package:blacknet/res/index.dart';
import 'package:blacknet/routers/routers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
// theme
import 'package:blacknet/theme/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// widgets
import 'package:blacknet/widgets/index.dart';
// 
import 'package:flustars/flustars.dart';
import 'package:flutter/services.dart';
// event
import 'package:blacknet/events/events.dart';

class TxDetailPage extends StatefulWidget {
  @override
  _TxDetailPage createState() => _TxDetailPage();
}

class _TxDetailPage extends State<TxDetailPage> with AutomaticKeepAliveClientMixin<TxDetailPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  ScrollController _scrollController = ScrollController();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  StateType _stateType = StateType.empty;

  BlnTxns _tx;

  BlnTxns _txnew;

  String _txhash;

  bool _isPending = false;
  
  @override
  void initState() {
    super.initState();
  }

  // 刷新
  Future<Null> _onRefresh() async {
    if(_txhash == null){
      return;
    }
    var tx = await Api.getTx(_txhash);
    if(mounted){
      setState(() {
        if(tx.seq != null){
          _txnew = tx;
          eventBus.fire(TxListRefreshEvent(_txnew));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    TxDetailPageArguments args = ModalRoute.of(context).settings.arguments;
    _tx = args.bln;
    _tx.hash = _tx.txid;
    _txhash = _tx.txid;
    if(_txnew != null){
      _tx = _txnew;
    }
    _isPending = false;
    if(_tx.seq == null){
      // 定时刷新获取数据
      _isPending = true;
      _onRefresh();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).detail),
        elevation: 0,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: NotificationListener(
              child: RefreshIndicator(
                //下拉刷新触发方法
                onRefresh: _onRefresh,
                child: CustomScrollView(
                  controller: _scrollController,
                  key: PageStorageKey<String>("txdetail"),
                  physics: AlwaysScrollableScrollPhysics(),
                  slivers: <Widget>[
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(horizontal: 0.0),
                      sliver: _stateType == StateType.loading ? SliverFillRemaining(child: StateLayout(type: _stateType)) :
                      SliverList(
                        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                          return index < 1 ?  _buildCard(context) : MoreWidget(1, false, 1000000000);
                        },
                        childCount: 2),
                      ),
                    )
                  ],
                )
              )
            )
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 40),
            child: Center(child: Text(S.of(context).detailBottomText))
          )
        ]
      )
    );
  }

  Widget _buildCard(BuildContext context) {
    DateTime now = _tx.time == null ? DateTime.now() :DateTime.fromMicrosecondsSinceEpoch(int.parse(_tx.time)*1000*1000);
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      child: new Card(
        elevation: 3.0,  //设置阴影
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(14.0))),  //设置圆角
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: new Column(  // card只能有一个widget，但这个widget内容可以包含其他的widget
            children: [
              new ListTile(
                contentPadding: const EdgeInsets.only(top: 20),
                title: new Column( 
                  children: <Widget>[
                    Center(child: Container(
                      width: 60,
                      height: 60,
                      child: Icon(_isPending? Icons.more_horiz :Icons.check_circle, color: Theme.of(context).accentColor, size: 60),
                    )),
                    // Center(child: _isPending ? Text('转账中') : Text('转账成功'))
                  ],
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Center(child: Text(formatDate(now)))
                )
              ),
              new Divider(),
              new ListTile(
                title: new Text(fomartBalance(_tx.data.amount)+ ' BLN'),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).amount),
                )
              ),
              _tx.fee > 0 ? new ListTile(
                title: new Text(fomartBalance(_tx.fee.toDouble())+ ' BLN'),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).fee)
                )
              ) : Gaps.empty,
              _buildTo(_tx),
              _buildFrom(_tx),
              new Divider(),
              _tx.txid != null ? new ListTile(
                title: Wrap(
                  spacing: 2.0,
                  children: <Widget>[
                    Text(_tx.txid != null ? _tx.txid.toString() : ""),
                    Icon(Icons.content_copy, size: 14)
                  ],
                ),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).hash),
                ),
                onTap: _tx.txid == null ? null : (){
                  Clipboard.setData(new ClipboardData(text: _tx.txid)).then((v){
                    Toast.show(S.of(context).copySuccess);
                  });
                }
              ): Gaps.empty,
              new ListTile(
                title: _tx.data.blockHeight != null ? Text(_tx.data.blockHeight.toString()) : Align(alignment: Alignment.centerLeft, child: const CupertinoActivityIndicator()),
                leading: Container(
                  width: 70,
                  child: Text(S.of(context).block),
                )
              ),
            ],
          )
        )
      )
    );
  }

  Widget _buildTo(BlnTxns _tx){
    var to = _tx.data.to != null ? _tx.data.to.toString() : "";
    if(_tx.type == 254){
      to = _tx.from;
    }
    return new ListTile(
      title: Wrap(
        spacing: 2.0,
        children: <Widget>[
          Text(to),
          Icon(Icons.content_copy, size: 14)
        ]
      ),
      leading: Container(
        width: 70,
        child: Text(S.of(context).toAddress),
      ),
      onTap: (){
        Clipboard.setData(new ClipboardData(text: to)).then((v){
          Toast.show(S.of(context).copySuccess);
        });
      }
    );
  }

  Widget _buildFrom(BlnTxns _tx){
    var from = _tx.from != null ? _tx.from.toString() : "";
    if(_tx.type == 254){
      from = "Pos Gennerate";
    }
    return new ListTile(
      title: Wrap(
        spacing: 2.0,
        children: <Widget>[
          Text(from),
          _tx.type == 254 ? Gaps.empty :Icon(Icons.content_copy, size: 14)
        ],
      ),
      leading: Container(
        width: 70,
        child: Text(S.of(context).fromAddress),
      ),
      onTap: _tx.type == 254 ? null :(){
        Clipboard.setData(new ClipboardData(text: from)).then((v){
          Toast.show(S.of(context).copySuccess);
        });
      }
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}