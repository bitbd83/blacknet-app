export 'package:blacknet/pages/transfer/txdetail_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// models
import 'package:blacknet/models/index.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// pages
import 'package:blacknet/pages/transfer/cancellease_page.dart';
import 'package:blacknet/pages/transfer/lease_page.dart';
import 'package:blacknet/pages/transfer/transfer_page.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';

class TransferIndexPage extends StatefulWidget {
  @override
  _TransferIndexPage createState() => _TransferIndexPage();
}

class _TransferIndexPage extends State<TransferIndexPage> with AutomaticKeepAliveClientMixin<TransferIndexPage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => false;

  TabController _tabController;//tab控制器

  List<String> tabs = new List();

  @override
  void initState() {
    super.initState();
    tabs= <String>[
      "transfer",
      "lease",
      "cancelLease"
    ];
    _tabController = TabController(vsync: this, length: tabs.length);
  }

  PageController _pageController = PageController(initialPage: 0);
  _onPageChange(int index) async{
    _tabController.animateTo(index);
  }

  @override
  Widget build(BuildContext context) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          title: new TabBar(
            isScrollable: true,
            controller: _tabController,
            labelPadding: EdgeInsets.all(12.0),
            indicatorSize: TabBarIndicatorSize.label,
            tabs: tabs.map((String text){
              return new Tab(
                text: S.of(context).blnType(text)
              );
            }).toList(),
            onTap: (index){
              if (!mounted){
                return;
              }
              _pageController.jumpToPage(index);
            }
          )
        ),
        body: NestedScrollView(
          key: const Key('transfer'),
          physics: ClampingScrollPhysics(),
          headerSliverBuilder: (context, boxIsScrolled) {
            return [];
          },
          body: PageView.builder(
            key: const Key('pageView'),
            itemCount: tabs.length,
            onPageChanged: _onPageChange,
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (_, index) {
              switch (tabs[index]) {
                case "lease":
                  return LeasePage();
                  break;
                case "cancelLease":
                  return CancelLeasePage(address: bln.address);
                  break;
                default:
                  return TransferPage();
              }
            }
          ) 
        )
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    super.dispose();
  }
}


// class TabLayout extends StatelessWidget {
//   List<String> children = [];
//   TabLayout({ this.children });
//   @override
//   Widget build(BuildContext context) {
//     return new TabBar(
//       isScrollable: true,
//       labelPadding: EdgeInsets.all(12.0),
//       indicatorSize: TabBarIndicatorSize.label,
//       tabs: children
//           .map((String text){
//             return new Tab(
//               text: S.of(context).blnType(text)
//             );
//           }).toList()
//     );
//   }
// }

// class TabBarViewLayout extends StatelessWidget {
//   List<TabPage> children = [];
//   TabBarViewLayout({ this.children });
//   @override
//   Widget build(BuildContext context) {
//     return new TabBarView(
//         children: children.map((TabPage tp) {
//           return tp.widget;
//     }).toList());
//   }
// }

// class TabPage {
//   final Tab tab;
//   final Widget widget;
//   TabPage({this.tab, this.widget});
// }