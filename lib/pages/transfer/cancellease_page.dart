import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
import 'package:blacknet/provider/tx.dart';
// theme
import 'package:blacknet/theme/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// widgets
import 'package:blacknet/widgets/index.dart';

class CancelLeasePage extends StatefulWidget {
  const CancelLeasePage({
    Key key,
    @required this.address,
  }): super(key: key);
  final String address;
  @override
  _CancelLeasePage createState() => _CancelLeasePage();
}

class _CancelLeasePage extends State<CancelLeasePage> with AutomaticKeepAliveClientMixin<CancelLeasePage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => true;

  ScrollController _scrollController = ScrollController();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();
  List<BlnLease> _list;
  double fee = 0.001;
  double amount;
  String to;
  String msg;
  bool encrypted = false;
  int height=0;
  StateType _stateType = StateType.empty;
  String _address;
  
  @override
  void initState() {
    super.initState();
    _address = widget.address;
    // _address = "blacknet1gqcgrut705k03vwrmvrqhg689gvqxtle4sly434fan09sg0kz2yszqa8ug";
    _onRefresh();
  }

  void _cancelLease(BuildContext context1, BlnLease bl) {
    Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
    showDialog(
      context: context1,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return PasswordPayDialog(
          onValidation: (List<String> codes){
            if(bln.password == codes.join()){
              return true;
            }
            return false;
          },
          onCallback: (bool success){
            if(success){
              _list.remove(bl);
              // _onRefresh();
              Provider.of<AccountProvider>(context).setCancelLease(_list);
            }
          },
          onPressed: () async{
            Api.cancelLease(bln.mnemonic, fee, bl.amount.toDouble() / 10e8, bln.address, bl.height).then((res){
              if(res.code == 200){
                _verificationNotifier.add(true);
              }else{
                _verificationNotifier.add(false);
              }
            });
          },
          shouldTriggerVerification: _verificationNotifier.stream,
        );
      }
    );
  }

  Widget _renderRow(BuildContext context, BlnLease bl) {
    return new ListTile(
      contentPadding: ThemeLayout.padding(10, 5, 10, 5),
      // subtitle: Text(shortAddress(bl.publicKey, 12)),
      title: Text(fomartBalance(bl.amount.toDouble())+ " BLN", style: Theme.of(context).textTheme.display2),
      // subtitle: Text(bl.height.toString()),
      leading: Container(width: 80, child: Column(children: <Widget>[
        Align(child: Text(bl.height.toString()), alignment: Alignment.centerLeft)
      ],
        mainAxisAlignment: MainAxisAlignment.center
      )),
      trailing: new IconButton( // action button
        // color: Theme.of(context).textTheme.display3.color,
        icon: Icon(Icons.cancel),
        onPressed: () {
          _cancelLease(context, bl);
        }
      ),
    );
  }

  // 刷新
  Future<Null> _onRefresh() async {
    var ls = await Api.outLeases(_address);
    if(mounted){
      Provider.of<AccountProvider>(context).setCancelLease(ls);
    }
  }

  @override
  Widget build(BuildContext context) {
    _list = Provider.of<AccountProvider>(context).getCancelLease();
    return NotificationListener(
      child: RefreshIndicator(
        //下拉刷新触发方法
        onRefresh: _onRefresh,
        child: CustomScrollView(
          controller: _scrollController,
          key: PageStorageKey<String>("cancellease"),
          physics: AlwaysScrollableScrollPhysics(),
          slivers: <Widget>[
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 0.0),
              sliver: _list == null || _list.isEmpty ? SliverFillRemaining(child: StateLayout(type: _stateType)) :
              SliverList(
                delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                  return index < _list.length ?  _renderRow(context, _list[index]) : MoreWidget(_list.length, false, 1000000000);
                },
                childCount: _list.length + 1),
              ),
            )
          ],
        )
      )
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}