import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// provider
import 'package:provider/provider.dart';
import 'package:blacknet/provider/account.dart';
// widgets
import 'package:blacknet/widgets/index.dart';
// utils
import 'package:blacknet/utils/index.dart';
// models
import 'package:blacknet/models/index.dart';
// apis
import 'package:blacknet/components/api/api.dart';
// i18n
import 'package:blacknet/i18n/i18n.dart';
// event
import 'package:blacknet/events/events.dart';
// router
import 'package:blacknet/routers/routers.dart';

class LeasePage extends StatefulWidget {
  @override
  _LeasePage createState() => _LeasePage();
}

class _LeasePage extends State<LeasePage> with AutomaticKeepAliveClientMixin<LeasePage>, SingleTickerProviderStateMixin{

  @override
  bool get wantKeepAlive => false;

  final TextEditingController _amountController = TextEditingController();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  final _formKey = GlobalKey<FormState>();
  
  double fee = 0.001;
  double amount;
  String to;
  String msg;
  bool encrypted = false;
  
  @override
  void initState() {
    super.initState();
  }

  bool _validate(){
    return to!=null && amount !=null && _formKey.currentState.validate() && validatorAddress(to) && validatorBalance(amount.toString());
  }

  _reset(){
     fee = 0.001;
     amount = null;
     to = null;
     msg = null;
     encrypted = false;
  }

  void _forSubmitted(BuildContext context1) {
    var _form = _formKey.currentState;
    if (_form.validate()) {
      _form.save();
      Bln bln = Provider.of<AccountProvider>(context).getCurrentBln();
      BlnTxns _tx = BlnTxns();
      showDialog(
        context: context1,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return PasswordPayDialog(
            onValidation: (List<String> codes){
              if(bln.password == codes.join()){
                return true;
              }
              return false;
            },
            onCallback: (bool success){
              if(success){
                //tx
                _tx.data = BlnTxnsData();
                _tx.data.amount = (amount * 1e8).toDouble();
                _tx.from = bln.address;
                _tx.data.to = to;
                _tx.fee = (fee * 1e8).toInt();
                _tx.type = 2;
                _tx.time = (DateTime.now().microsecondsSinceEpoch/1e6).toInt().toString();
                //balance
                BlnBalance balance = Provider.of<AccountProvider>(context).getBalance();
                balance.balance -= _tx.data.amount.toInt();
                // reset
                _form.reset();
                _amountController.text = '';
                setState(() {
                  _reset();
                });
                // notify
                Provider.of<AccountProvider>(context).setBalance(balance);
                eventBus.fire(TxListRefreshEvent(_tx));
                // to detail
                // 跳转页面
                Navigator.pushNamed(context, Routes.txDetail,
                  arguments: TxDetailPageArguments(
                    bln: _tx
                  )
                );
              }
            },
            onPressed: () async{
              Api.lease(bln.mnemonic, fee, amount, to).then((res){
                if(res.code == 200){
                  _tx.txid = res.body;
                  _verificationNotifier.add(true);
                }else{
                  _verificationNotifier.add(false);
                }
              });
            },
            shouldTriggerVerification: _verificationNotifier.stream,
          );
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double realAmount = realBalance(Provider.of<AccountProvider>(context).getBalance().balance.toDouble());
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: SingleChildScrollView(
              key: const Key('transfer'),
              // padding: const EdgeInsets.symmetric(vertical: 16.0),
              physics: BouncingScrollPhysics(),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(bottom: 16),
                      padding: const EdgeInsets.only(bottom: 10),
                      color: Theme.of(context).cardColor,
                      child:  ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(S.of(context).amount),
                            FlatButton(
                              padding: const EdgeInsets.all(0),
                              onPressed: () {
                                if(realAmount > 0.0){
                                  _amountController.text = realAmount.toString();
                                }
                              },
                              child: Text(
                                fomartBalance(Provider.of<AccountProvider>(context).getBalance().balance.toDouble())+' BLN',
                                style: TextStyle(color: Theme.of(context).textTheme.display3.color)
                              )
                            )
                          ]
                        ),
                        subtitle: Container(
                          child: TextFormField(
                            key: Key("amount"),
                            autocorrect: true,
                            controller: _amountController,
                            autofocus: false,
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(0),
                              border: InputBorder.none,
                              // focusedBorder: !_validate && bln.mnemonic != null ? OutlineInputBorder(borderSide: BorderSide(
                              //     color: Colors.redAccent,
                              //     width: 0.8
                              // )) : InputBorder.none,
                              hintText: S.of(context).amountInput
                            ),
                            onSaved: (val) {
                              amount = double.parse(val);
                            },
                            onChanged: (value) {
                              try {
                                amount = double.parse(value);
                              } catch (e) {
                                amount = -1;
                              }
                              setState(() {});
                            },
                            validator: (value) {
                              if(amount==null){
                                return null;
                              }
                              if(!validatorBalance(value)){
                                return S.of(context).amountInputCorrect;
                              }
                              if(double.parse(value) > realAmount){
                                return S.of(context).amountInputCorrect;
                              }
                              return null;
                            },
                            autovalidate: true
                          )
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 16),
                      padding: const EdgeInsets.only(bottom: 10),
                      color: Theme.of(context).cardColor,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(S.of(context).address),
                                FlatButton(
                                  padding: const EdgeInsets.all(0),
                                  child: Text("")
                                )
                              ]
                            ),
                            subtitle: Container(
                              child: TextFormField(
                                key: Key("to"),
                                autofocus: false,
                                autocorrect: true,
                                decoration: InputDecoration(
                                  // contentPadding: const EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 5),
                                  contentPadding: const EdgeInsets.all(0),
                                  border: InputBorder.none,
                                  // focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                                  hintText: S.of(context).addressInput
                                ),
                                onSaved: (val) {
                                  to = val;
                                },
                                onChanged: (value) {
                                  if(!validatorAddress(value)){
                                    return null;
                                  }
                                  setState(() {
                                    to = value;
                                  });
                                },
                                autovalidate: true,
                                validator: (value) {
                                  if(to==null){
                                    return null;
                                  }
                                  if(!validatorAddress(value)){
                                    return S.of(context).addressInputCorrect;
                                  }
                                  return null;
                                }
                              )
                            ),
                          )
                        ]
                      )
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 16),
                      padding: const EdgeInsets.only(bottom: 10),
                      color: Theme.of(context).cardColor,
                      child:  ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(S.of(context).fee),
                            FlatButton(
                              padding: const EdgeInsets.all(0),
                              child: Text(
                                fee.toStringAsFixed(4)+' BLN',
                                style: TextStyle(color: Theme.of(context).accentColor)
                              )
                            )
                          ]
                        ),
                        subtitle: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Icon(Icons.directions_walk, color: Theme.of(context).accentColor)
                              ),
                              Expanded(
                                flex: 8,
                                child: Slider(
                                  value: fee,
                                  min: 0.0001,
                                  max: 0.01,
                                  onChanged: (newValue) {
                                    setState(() {
                                      fee = newValue;
                                    });
                                  }
                                )
                              ),
                              Expanded(
                                flex: 1,
                                child: Icon(Icons.directions_run, color: Theme.of(context).accentColor)
                              )
                            ]
                          )
                        ),
                      ),
                    )
                  ],
                )
              )
            )
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16),
            child: Container(
              margin: const EdgeInsets.only(top: 25),
              child: MyButton(
                key: Key("send"),
                text: S.of(context).transfer,
                onPressed: !_validate() ? null: () async {
                  _forSubmitted(context);
                }
              )
            )
          )
        ],
      )
    );
  }

  //当整个页面dispose时，记得把控制器也dispose掉，释放内存
  @override
  void dispose() {
    // pass
    _verificationNotifier.close();
    _amountController.dispose();
    super.dispose();
  }
}