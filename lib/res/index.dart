export 'package:blacknet/res/colors.dart';

export 'package:blacknet/res/dimens.dart';


export 'package:blacknet/res/gaps.dart';

export 'package:blacknet/res/styles.dart';


export 'package:blacknet/res/resources.dart';